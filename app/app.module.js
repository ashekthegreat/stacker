(function () {
    angular.module("stacker", ['ui.router', 'nsPopover', 'perfect_scrollbar', 'ui.slider', 'angular-storage', 'ngFileUpload', 'angular-growl', 'angular-loading-bar'])
        .factory('stExceptionInterceptor', [
            '$q',
            '$rootScope',
            'store',
            function ($q, $rootScope, store) {
                return {
                    request: function (config) {
                        // TODO: push token in header
                        var auth = store.get("auth");
                        if (auth && auth.token) {
                            config.headers.token = store.get("auth").token;
                        }
                        //console.log(config);
                        return config;
                    },
                    responseError: function (response) {
                        console.log("Response error");
                        console.log(response);
                        var status = response.status;
                        if (status === 401) {
                            $rootScope.$broadcast('stUnauthorized');
                            //$state.go("login");
                        } else if (status === 404) {
                            $rootScope.$broadcast('stNotFound');
                        } else if (status === 500) {
                            $rootScope.$broadcast('stError');
                        }

                        return $q.reject(response);
                    }
                }
            }
        ])
        .config([
            '$httpProvider',
            function ($httpProvider) {
                $httpProvider.interceptors.push('stExceptionInterceptor');
            }
        ])
        .config(['growlProvider', function (growlProvider) {
            growlProvider.globalTimeToLive(5000);
            growlProvider.globalDisableCountDown(true);
        }])
        .run([
            '$rootScope',
            'authFactory',
            function ($rootScope, authFactory) {
                $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
                    $rootScope.projectId = toParams.projectId;
                    $rootScope.versionId = toParams.versionId;
                    if (!authFactory.checkAccess(toState, toParams)) {
                        event.preventDefault();
                    }
                });

                $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
                    $rootScope.showNav = !!toState.showNav;
                });
            }
        ]);
}());
