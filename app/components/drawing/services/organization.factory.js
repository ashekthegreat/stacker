(function () {
    angular.module("stacker")
        .service("organizationFactory", organizationFactory);

    organizationFactory.$inject = ["$http", "$q", "$window", "MEASUREMENT_UNITS"];

    function organizationFactory($http, $q, $window, MEASUREMENT_UNITS) {
        var factory = {};
        factory.project = {};
        factory.org = {};
        factory.history = {
            undoStack: [],
            redoStack: [],
            isUndoOrRedo: false
        };

        factory.load = function (id, versionId) {
            return $http.get('backend/load_project.php?id=' + id).then(function (payload) {
                _.each(payload.data.versions, function (v) {
                    v.organization = JSON.parse(v.organization);
                });
                factory.history.undoStack = [];
                factory.history.redoStack = [];
                factory.project = payload.data;
                if (factory.project.versions.length) {
                    factory.project.version = factory.project.versions[0];
                }
                if (versionId) {
                    var version = _.find(factory.project.versions, function (v) {
                        return v.id == versionId;
                    });
                    if (version) {
                        factory.project.version = version;
                    }
                }
                factory.org = factory.project.version.organization;

                return payload.data;
            });
        };

        factory.loadFromData = function (organization) {
            factory.org = organization;
            factory.project.version.organization = organization;
        };

        factory.save = function (name, note) {
            var version = factory.project.version;
            if (typeof name != 'undefined') {
                // saving a new version
                version.id = 0;
                version.name = name;
                version.note = note;
            }
            return $http.post('backend/save_data.php', {version: version}).
                success(function (data, status, headers, config) {
                    // saving successful
                    if (data != 0) {
                        // update the version id
                        factory.project.version.id = data;
                    }
                }).
                error(function (data, status, headers, config) {
                    console.log(data);
                });

        };

        factory.updateHistory = function () {
            var limit = 20;
            factory.history.undoStack.push(angular.copy(factory.org));
            if (factory.history.undoStack.length > limit) {
                factory.history.undoStack.splice(0, factory.history.undoStack.length - limit);
            }
            if (!factory.history.isUndoOrRedo) {
                factory.history.redoStack.length = 0;
            }
            factory.history.isUndoOrRedo = false;
        };

        factory.undo = function () {
            if (factory.history.undoStack.length > 1) {
                factory.history.redoStack.push(factory.history.undoStack.pop());
                factory.org = factory.history.undoStack.pop();
                factory.history.isUndoOrRedo = true;
            }
        };

        factory.redo = function () {
            if (factory.history.redoStack.length) {
                factory.org = factory.history.redoStack.pop();
                factory.history.isUndoOrRedo = true;
            }
        };

        factory.getUnit = function (code) {
            return _.find(factory.org.units, function (unit, i) {
                return unit && unit.code == code;
            });
        };

        factory.isUnit = function (code) {
            return !_.isEmpty(factory.getUnit(code));
        };

        factory.deleteUnit = function (code) {
            // delete any usage of unit at any floor
            _.each(factory.org.buildings, function (building) {
                _.each(building.floors, function (floor) {
                    floor.units = _.reject(floor.units, function (u) {
                        return u == code;
                    });
                });
            });

            // delete unit itself
            factory.org.units = _.reject(factory.org.units, function (u) {
                return u.code == code;
            });
        };

        factory.getFloorByTitle = function (building, floorTitle) {
            return _.find(building.floors, function (floor) {
                return floor.title == floorTitle;
            });
        };

        factory.getFloorByUnitCode = function (code) {
            var targetFloor;
            _.each(factory.org.buildings, function (building) {
                _.each(building.floors, function (floor) {
                    if (floor.units.indexOf(code) > -1) {
                        targetFloor = floor;
                    }
                });
            });
            return targetFloor;
        };

        factory.getMaxFloorAreaNeeded = function (floor) {
            var maxFloorAreaNeeded = +floor.void + +floor.core;
            // loop through all the floor units
            _.each(floor.units, function (unitCode) {
                maxFloorAreaNeeded += +factory.getUnit(unitCode).area;
            });

            var maxUnitArea = 0;
            _.each(factory.org.units, function (unit) {
                maxUnitArea = (unit.area > maxUnitArea) ? unit.area : maxUnitArea;
            });
            maxFloorAreaNeeded += +maxUnitArea;
            return maxFloorAreaNeeded;
        };

        factory.remainingFloorArea = function (floor) {
            var remaining = floor.area - floor.void - floor.core;
            _.each(floor.units, function (code) {
                var unit = factory.getUnit(code);
                if (unit) {
                    remaining -= unit.area;
                }
            });
            return remaining;
        };

        factory.totalBuildingArea = function (building) {
            var total = 0;
            _.each(building.floors, function (floor) {
                total += floor.area - floor.void - floor.core;
            });
            return total;
        };

        factory.remainingBuildingArea = function (building) {
            var remaining = 0;
            _.each(building.floors, function (floor) {
                remaining += factory.remainingFloorArea(floor);
            });
            return remaining;
        };

        factory.chopOverHanging = function (floor) {
            var len = floor.units.length;
            for (var i = 0; i < len; i++) {
                var unit = factory.getUnit(_.last(floor.units));
                var rfa = factory.remainingFloorArea(floor);
                if (rfa < 0) {
                    // overhanging units are there
                    if (unit.area + rfa <= 0) {
                        // revert item
                        floor.units = _.initial(floor.units);
                        unit.isUsed = false;
                    } else {
                        factory.chopUnit(unit, floor, unit.area + rfa);
                        break;
                    }
                } else {
                    // no overhanging units
                    break;
                }
            }
        };

        factory.chopUnit = function (unit, floor, area) {
            var unit1 = angular.copy(unit);
            unit1.area = area;
            unit1.seat = Math.round((unit1.area * unit.seat) / unit.area);

            var unit2 = factory.createUnitFromExisting(unit);
            unit2.area = unit.area - area;
            unit2.seat = unit.seat - unit1.seat;
            unit2.isUsed = false;

            var originalCode = unit.code;

            // update first unit
            angular.extend(unit, unit1);

            // update 2nd unit
            var index = _.findIndex(factory.org.units, function (u) {
                return (u.code == unit1.code);
            });
            if (index > -1) {
                factory.org.units.splice(index + 1, 0, unit2);
            }

            // update floor
            if (!_.isEmpty(floor)) {
                var floorIndex = floor.units.indexOf(originalCode);
                if (floorIndex > -1) {
                    floor.units[floorIndex] = unit1.code;
                }
            }
        };

        factory.addUnit = function (unit) {
            factory.org.units.push(unit);
        };

        factory.changeMeasurementUnit = function (measurementUnitTitle) {
            if (factory.org.measurementUnit != measurementUnitTitle) {
                // change all unit values
                var sourceFactor = 1;
                var targetFactor = 1;
                _.each(MEASUREMENT_UNITS, function (unit) {
                    if (unit.title == factory.org.measurementUnit) {
                        sourceFactor = unit.factor;
                    } else if (unit.title == measurementUnitTitle) {
                        targetFactor = unit.factor;
                    }
                });
                var ratio = targetFactor / sourceFactor;

                if (ratio > 0 && ratio != 1) {
                    // change units
                    _.each(factory.org.units, function (unit) {
                        unit.area *= ratio;
                    });

                    // change buildings
                    _.each(factory.org.buildings, function (building) {
                        _.each(building.floors, function (floor) {
                            floor.area *= ratio;
                            floor.offset *= ratio;
                            floor.void *= ratio;
                            floor.core *= ratio;
                        });
                    });
                }

                factory.org.measurementUnit = measurementUnitTitle;
                return ratio;
            }
            return 1;
        };

        factory.createUnitFromExisting = function (existingUnit) {
            var unit = angular.copy(existingUnit);

            // match(/^(\D+)(\d+)$/)
            //var match = unit.code.match(/^([a-zA-Z]+)([0-9]+)$/);
            var match = unit.code.match(/^(\D+)(\d+)$/);
            if (match) {
                unit.code = match[1];
            }
            match = unit.name.match(/^(\D+)(\d+)$/);
            if (match) {
                unit.name = match[1];
            } else {
                unit.name = unit.name + "-";
            }

            var temp;
            for (var i = 1; i <= 20; i++) {
                temp = _.find(factory.org.units, function (loopUnit) {
                    return ((loopUnit.name == unit.name + "" + i) || (loopUnit.code == unit.code + "" + i));
                });
                if (!temp) {
                    unit.name = unit.name + "" + i;
                    unit.code = unit.code + "" + i;
                    return unit;
                }
            }
            var timeStamp = new Date().getTime();
            unit.name = unit.name + "" + timeStamp;
            unit.code = unit.code + "" + timeStamp;
            return unit;
        };

        return factory;
    }
}());