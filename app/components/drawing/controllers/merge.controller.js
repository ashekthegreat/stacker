(function () {
    angular.module("stacker")
        .controller("MergeController", MergeController);

    MergeController.$inject = ["$scope", "organizationFactory"];

    function MergeController($scope, organizationFactory) {

        $scope.$on('mergeUnit', function (event, args) {
            $scope.floor = args.floor;
            $scope.unit1 = args.units.unit1;
            $scope.unit2 = args.units.unit2;

            $scope.unit = angular.copy($scope.unit1);
            $scope.unit.area = $scope.unit1.area + $scope.unit2.area;
            $scope.unit.seat = $scope.unit1.seat + $scope.unit2.seat;
            $scope.unit.name = $scope.unit1.name;
            $scope.unit.code = $scope.unit1.code;
            $scope.save();
        });

        $scope.save = function () {
            // update first unit
            angular.extend($scope.unit1, $scope.unit);

            // update 2nd unit
            organizationFactory.deleteUnit($scope.unit2.code);

            $scope.close();
        };

        $scope.close = function () {
            $scope.unit = undefined;
            $scope.unit1 = undefined;
            $scope.unit2 = undefined;
        }

    }
}());