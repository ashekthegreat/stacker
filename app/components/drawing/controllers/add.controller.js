(function () {
    angular.module("stacker")
        .controller("AddController", AddController);

    AddController.$inject = ["$scope", "organizationFactory"];

    function AddController($scope, organizationFactory) {

        $scope.errors = {
            name: {
                isValid: true,
                message: ""
            },
            code: {
                isValid: true,
                message: ""
            },
            area: {
                isValid: true,
                message: ""
            }
        };
        $scope.validation = {
            isValid: true,
            message: ""
        };

        function checkValid(errorCode) {
            // first check current element has errors
            if(errorCode){
                $scope.validation = $scope.errors[errorCode];
            }

            // now check all the other elements
            if ($scope.validation.isValid) {
                _.find($scope.errors, function (error) {
                    if (!error.isValid) {
                        $scope.validation = error;
                        return true;
                    }
                });
            }
        }

        function clearErrors() {
            _.each($scope.errors, function (key, val) {
                val.isValid = true;
                val.message = "";
            });
            $scope.validation.isValid = true;
            $scope.validation.message = "";
        }

        $scope.$on('addUnit', function () {
            $scope.unit = {
                "name": "",
                "code": "",
                "area": 0,
                "isShowArea": true,
                "seat": 0,
                "isShowSeat": false,
                "areaPerSeat": 0,
                "isUsed": false,
                "fill": {
                    "startColor": "#cccccc",
                    "endColor": "#999999"
                },
                "font": {
                    "color": "#ffffff",
                    "fontSize": "11px"
                }
            };
            clearErrors();
            $scope.validation.isValid = false;
            $scope.errors.name.isValid = false;
            $scope.errors.code.isValid = false;
            $scope.errors.area.isValid = false;
        });

        $scope.save = function () {
            if (!$scope.validation.isValid) {
                return;
            }

            if (isNaN($scope.unit.seat)) {
                $scope.unit.seat = "";
            }
            organizationFactory.addUnit($scope.unit);
            $scope.close();
        };

        $scope.close = function () {
            $scope.unit = undefined;
        };

        $scope.changeName = function (name) {
            if ((typeof name == "undefined") || name == "") {
                $scope.errors.name.isValid = false;
                $scope.errors.name.message = "Unit name is required";
            } else {
                $scope.errors.name.isValid = true;
                $scope.errors.name.message = "";
            }
            checkValid("name");
        };

        $scope.changeCode = function (code) {
            if ((typeof code == "undefined") || code == "") {
                $scope.errors.code.isValid = false;
                $scope.errors.code.message = "Unit code is required";
            } else if (organizationFactory.isUnit(code)) {
                $scope.errors.code.isValid = false;
                $scope.errors.code.message = "Unit code must be unique";
            } else {
                $scope.errors.code.isValid = true;
                $scope.errors.code.message = "";
            }
            checkValid("code")
        };

        $scope.changeArea = function () {
            $scope.isValid = !((typeof $scope.unit.area == "undefined") || $scope.unit.area < 1);
            if ((typeof $scope.unit.area == "undefined") || $scope.unit.area < 1) {
                $scope.errors.area.isValid = false;
                $scope.errors.area.message = "Unit area must be a valid number";
            } else {
                $scope.errors.area.isValid = true;
                $scope.errors.area.message = "";
            }
            checkValid("area");
        };

    }
}());