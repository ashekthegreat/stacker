(function () {
    angular.module("stacker")
        .controller("EditController", EditController);

    EditController.$inject = ["$scope", "organizationFactory"];

    function EditController($scope, organizationFactory) {

        $scope.errors = {
            name: {
                isValid: true,
                message: ""
            },
            code: {
                isValid: true,
                message: ""
            },
            area: {
                isValid: true,
                message: ""
            }
        };
        $scope.validation = {
            isValid: true,
            message: ""
        };

        function checkValid(errorCode) {
            // first check current element has errors
            $scope.validation = $scope.errors[errorCode];

            // now check all the other elements
            if ($scope.validation.isValid) {
                _.find($scope.errors, function (error) {
                    if (!error.isValid) {
                        $scope.validation = error;
                        return true;
                    }
                });
            }
        }

        function clearErrors() {
            _.each($scope.errors, function (key, val) {
                val.isValid = true;
                val.message = "";
            });
            $scope.validation.isValid = true;
            $scope.validation.message = "";
        }

        $scope.$on('editUnit', function (event, args) {
            $scope.floor = args.floor;
            $scope.originalUnit = args.unit;
            $scope.unit = angular.copy($scope.originalUnit);
            clearErrors();
        });

        $scope.save = function () {
            if (!$scope.validation.isValid) {
                return;
            }
            var originalCode = $scope.originalUnit.code;
            angular.extend($scope.originalUnit, $scope.unit);

            if (isNaN($scope.originalUnit.seat)) {
                $scope.originalUnit.seat = "";
            }

            // update floor
            if ($scope.floor) {
                var floorIndex = $scope.floor.units.indexOf(originalCode);
                if (floorIndex > -1) {
                    $scope.floor.units[floorIndex] = $scope.unit.code;
                }
            }
            $scope.close();

        };

        $scope.close = function () {
            $scope.originalUnit = undefined;
            $scope.unit = undefined;
        };

        $scope.changeName = function (name) {
            if ((typeof name == "undefined") || name == "") {
                $scope.errors.name.isValid = false;
                $scope.errors.name.message = "Unit name is required";
            } else {
                $scope.errors.name.isValid = true;
                $scope.errors.name.message = "";
            }
            checkValid("name");
        };

        $scope.changeCode = function (code) {
            if ((typeof code == "undefined") || code == "") {
                $scope.errors.code.isValid = false;
                $scope.errors.code.message = "Unit code is required";
            } else if (code != $scope.originalUnit.code && organizationFactory.isUnit(code)) {
                $scope.errors.code.isValid = false;
                $scope.errors.code.message = "Unit code must be unique";
            } else {
                $scope.errors.code.isValid = true;
                $scope.errors.code.message = "";
            }
            checkValid("code")
        };

        $scope.changeArea = function () {
            $scope.isValid = !((typeof $scope.unit.area == "undefined") || $scope.unit.area < 1 || $scope.unit.area >= $scope.originalUnit.area);
            if ((typeof $scope.unit.area == "undefined") || $scope.unit.area < 1) {
                $scope.errors.area.isValid = false;
                $scope.errors.area.message = "Unit area must be a valid number";
            } else {
                $scope.errors.area.isValid = true;
                $scope.errors.area.message = "";
            }
            checkValid("area");
        };

    }
}());