(function () {
    angular.module("stacker")
        .controller("DrawingController", DrawingController);

    DrawingController.$inject = ["$scope", "$filter", "organizationFactory", "$window", "modals", "$state", "$stateParams", 'growl',"MEASUREMENT_UNITS"];

    function DrawingController($scope, $filter, organizationFactory, $window, modals, $state, $stateParams, growl, MEASUREMENT_UNITS) {

        // main data object
        $scope.measurementUnits = MEASUREMENT_UNITS;
        $scope.organization = {};
        $scope.history = {};
        // current active building
        $scope.activeBuilding = {};
        $scope.activeBuildingIndex = 0;

        // holder for merging units
        $scope.mergeUnits = {};
        // the ratio to use for area vs pixel calculation
        $scope.ratio = 1;
        $scope.isActionsVisible = false;
        $scope.measurementUnit = "";

        $scope.hoverUnit;
        $scope.hoverFloor;
        $scope.timer;

        $scope.isFooterVisible = true;
        $scope.isBuildingSummaryVisible = true;

        $scope.isDataDirty = false;

        $scope.isDirty = function () {
            return $scope.isDataDirty && organizationFactory.history.undoStack.length > 1;
        };

        var stopWatchingLocation = $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
            if ($scope.isDirty()) {
                event.preventDefault();
                
                var promise = modals.open("confirm", {
                        message: "Your data have not been saved yet. Would you like to save it now?"
                    }
                );

                promise.then(
                    function handleResolve(response) {
                        stopWatchingLocation();
                        organizationFactory.save().then(function () {
                            growl.success("Data saved successfully", {title: 'Saving Data'});
                            $state.go(toState);
                        });
                    },
                    function handleReject(error) {
                        // cancel
                        stopWatchingLocation();
                        $state.go(toState);
                    }
                );
            }
        });

        if ($stateParams.projectId && $stateParams.versionId) {
            loadData($stateParams.projectId, $stateParams.versionId);
        } else {
            //$state.go("projects");
        }

        function loadData(id, versionId) {

            organizationFactory.load(id, versionId)
                .then(function (organization) {
                    if (_.isEmpty(organizationFactory.org)) {
                        $state.go("projects");
                        return;
                    }
                    dataLoaded();
                    calculateRatio();
                    $scope.$watch(function () {
                        return organizationFactory.org
                    }, function () {
                        organizationFactory.updateHistory();
                        $scope.isDataDirty = true;
                        dataLoaded();
                    }, true);
                });
        }

        function dataLoaded() {
            $scope.project = organizationFactory.project;
            $scope.organization = organizationFactory.org;
            $scope.history = organizationFactory.history;
            $scope.measurementUnit = organizationFactory.org.measurementUnit;
            //console.log($scope.organization);
            if ($scope.organization.buildings.length) {
                if (!_.isEmpty($scope.activeBuilding)) {
                    $scope.activeBuilding = _.find($scope.organization.buildings, function (building) {
                        return (building.title == $scope.activeBuilding.title);
                    })
                } else {
                    $scope.activeBuilding = $scope.organization.buildings[$scope.activeBuildingIndex];
                }
            }
        }

        function calculateRatio() {
            var widthAvailable = ($window.innerWidth - 350) * 0.75;

            var maxFloorArea = 0;
            if (!_.isEmpty($scope.activeBuilding)) {
                _.each($scope.activeBuilding.floors, function (floor) {
                    maxFloorArea = (floor.area > maxFloorArea) ? floor.area : maxFloorArea;
                });
                $scope.ratio = widthAvailable / maxFloorArea;
            }
        }

        $scope.switchActiveBuilding = function (direction) {
            $scope.activeBuildingIndex += direction;
            $scope.activeBuilding = $scope.organization.buildings[$scope.activeBuildingIndex];
        };

        $scope.getUnitStyle = function (unit, floor) {
            var style = {};
            if (unit == "void") {
                style = {
                    width: (floor.void * $scope.ratio) + "px"
                }
            } else if (unit == "core") {
                style = {
                    width: (floor.core * $scope.ratio) + "px"
                }
            } else {
                style = {
                    "width": (unit.area * $scope.ratio - 1) + "px",
                    "background": "linear-gradient(to bottom,  " + unit.fill.startColor + " 0%," + unit.fill.startColor + " 30%," + unit.fill.endColor + " 30%," + unit.fill.endColor + " 100%)",
                    "color": unit.font.color,
                    "font-size": unit.font.fontSize
                };
                if ($scope.mergeUnits.unit1 && $scope.mergeUnits.unit1.code == unit.code) {
                    style["z-index"] = 0;
                }
            }
            return style;
        };

        $scope.getUnitExtraStyle = function (unit) {
            var unitPixelWidth = (unit.area * $scope.ratio);
            if (unitPixelWidth < 264) {
                return {
                    "left": unitPixelWidth + "px"
                }
            }
            return {};
        };

        $scope.getUnitPixelWidth = function (unit) {
            return (unit.area * $scope.ratio);
        };

        $scope.getUnitMeasurement = function (unit) {
            if(!unit){
                return "";
            }
            var measurements = [];
            if (unit.isShowSeat && unit.seat) {
                measurements.push(unit.seat + " ppl");
            }
            if (unit.isShowArea && unit.area) {
                measurements.push($filter('number')(unit.area, 0) + " " + $scope.organization.measurementUnit);
            }
            return measurements.join(" / ");
        };

        $scope.getOverhangingStyle = function (unit, floor) {
            var style={};
            if (!_.isEmpty(floor)) {
                var tempFloor = angular.copy(floor);
                tempFloor.units = _.first(tempFloor.units, tempFloor.units.indexOf(unit.code) + 1);
                var remaining = organizationFactory.remainingFloorArea(tempFloor);

                if( remaining < 0){
                    style = {
                        width: (Math.abs(remaining) * $scope.ratio) + "px"
                    }
                } else {
                    style = {
                        width: "0"
                    }
                }
            }
            return style;
        };

        $scope.getFloorStyle = function (floor) {
            return {
                width: (((floor.area) * $scope.ratio)) + "px",
                 left: (floor.offset * $scope.ratio) + "px"
            };
        };

        $scope.getFloorUnitContainerStyle = function (floor) {
            var w = organizationFactory.getMaxFloorAreaNeeded(floor);
            return {
                width: ((w * $scope.ratio) + 4) + "px"
            };
        };

        $scope.remainingFloorArea = function (floor) {
            return organizationFactory.remainingFloorArea(floor);
        };

        $scope.totalBuildingArea = function (building) {
            return organizationFactory.totalBuildingArea(building);
        };

        $scope.remainingBuildingArea = function (building) {
            return organizationFactory.remainingBuildingArea(building);
        };

        $scope.getFloorUnits = function (floor) {
            var floorUnits = [];
            _.each(floor.units, function (unitCode) {
                floorUnits.push(organizationFactory.getUnit(unitCode));
            });

            return floorUnits;
        };

        $scope.updateFloorUnits = function (floorTitle, units) {
            var floor = organizationFactory.getFloorByTitle($scope.activeBuilding, floorTitle);
            if (floor) {
                floor.units = units;
            }
        };

        $scope.changeUnitProperty = function (code, prop, val) {
            var unit = organizationFactory.getUnit(code);
            if (unit) {
                unit[prop] = val;
            }
        };

        $scope.insertUnitAfter = function (code, prevUnitCode) {
            //var targetUnit = organizationFactory.getUnit(code);
            var units = organizationFactory.org.units;

            var to = _.findIndex(units, {code: prevUnitCode}) + 1;

            var from = _.findIndex(units, {code: code});
            console.log("To: " + to);
            console.log("From: " + from);
            if(to<from){
                units.splice(to, 0, units.splice(from, 1)[0]);
            } else{
                units.splice(to-1, 0, units.splice(from, 1)[0]);
            }
        };

        $scope.removeUnit = function (unit, floor) {
            unit.isUsed = false;
            floor.units = _.reject(floor.units, function (floorUnit) {
                return unit.code == floorUnit;
            });
        };

        $scope.mergeStart = function (unit, floor) {
            $scope.mergeUnits.unit1 = unit;
        };

        $scope.mergeCancel = function () {
            $scope.mergeUnits.unit1 = undefined;
            $scope.mergeUnits.unit2 = undefined;
        };

        $scope.mergeUnit = function (unit, floor) {
            if ($scope.mergeUnits.unit1) {
                $scope.mergeUnits.unit2 = unit;
                $scope.$broadcast('mergeUnit', {units: $scope.mergeUnits, floor: floor});
                $scope.mergeUnits = {};
            } else {
                // show the unit details block
                $scope.setHoverUnit(unit, floor);
            }
        };

        $scope.splitUnit = function (unit, floor) {
            $scope.$broadcast('splitUnit', {unit: unit, floor: floor});
        };

        $scope.editUnit = function (unit, floor) {
            $scope.$broadcast('editUnit', {unit: unit, floor: floor});
        };

        $scope.deleteUnit = function (unit) {
            var promise = modals.open("confirm", {
                    message: "Are you sure you want to delete this Unit?"
                }
            );

            promise.then(
                function handleResolve(response) {
                    organizationFactory.deleteUnit(unit.code);
                },
                function handleReject(error) {
                    // cancel
                }
            );
        };

        $scope.addUnit = function () {
            $scope.$broadcast('addUnit');
        };

        $scope.chopOverHanging = function (floor) {
            organizationFactory.chopOverHanging(floor);
        };

        $scope.isOverHanging = function (unit, floor) {
            if (_.isEmpty(floor)) {
                return false;
            } else {
                var tempFloor = angular.copy(floor);
                tempFloor.units = _.first(tempFloor.units, tempFloor.units.indexOf(unit.code) + 1);
                return !!(organizationFactory.remainingFloorArea(tempFloor) < 0);
            }
        };

        $scope.setHoverUnit = function (unit, floor) {
            clearTimeout($scope.timer);
            $scope.hoverUnit = unit;
            $scope.hoverFloor = floor;
        };

        $scope.onUnitLeave = function (unit) {
            clearTimeout($scope.timer);
            $scope.timer = setTimeout(function () {
                $scope.hoverUnit = undefined;
                $scope.hoverFloor = undefined;
                $scope.$apply();
            }, 500);
        };

        $scope.goToHome = function () {
            var promise = modals.open("confirm", {
                    message: "Are you sure you want to leave this page and go to Project Lists?"
                }
            );

            promise.then(
                function handleResolve(response) {
                    $state.go("projects");
                },
                function handleReject(error) {
                    // cancel
                }
            );
        };

        $scope.changeMeasurementUnit = function (measurementUnit) {
            var ratio = organizationFactory.changeMeasurementUnit(measurementUnit);
            $scope.ratio /= ratio;
        };

        $scope.undo = function () {
            organizationFactory.undo();
            dataLoaded();
        };

        $scope.redo = function () {
            organizationFactory.redo();
            dataLoaded();
        };

        $scope.changeRatio = function (zoomType) {
            var multiplier = 1.25;
            if (zoomType == 'in') {
                $scope.ratio *= multiplier;
            } else if (zoomType == 'out') {
                $scope.ratio /= multiplier;
            }
        };

        $scope.reload = function () {
            // The .open() method returns a promise that will be either
            // resolved or rejected when the modal window is closed.
            var promise = modals.open("confirm", {
                    message: "Are you sure you want to reload your data? All unsaved changes will be lost."
                }
            );

            promise.then(
                function handleResolve(response) {
                    //organizationFactory.load($stateParams.projectId, $stateParams.versionId);
                    loadData($stateParams.projectId, $stateParams.versionId);
                },
                function handleReject(error) {
                    // cancel
                }
            );
        };

        $scope.save = function () {
            // The .open() method returns a promise that will be either
            // resolved or rejected when the modal window is closed.
            var promise = modals.open("confirm", {
                    message: "Are you sure you want to save your data?"
                }
            );

            promise.then(
                function handleResolve(response) {
                    organizationFactory.save().then(function () {
                        growl.success("Data saved successfully", {title: 'Saving Data'});
                        $scope.isDataDirty = false;
                    });
                },
                function handleReject(error) {
                    // cancel
                }
            );
        };

        $scope.saveAs = function () {
            // The .open() method returns a promise that will be either
            // resolved or rejected when the modal window is closed.
            var promise = modals.open("promptSave", {
                    message: "Give us a new name for this version"
                }
            );

            promise.then(
                function handleResolve(response) {
                    organizationFactory.save(response.name, response.note)
                        .then(function (response) {
                            growl.success("Data saved successfully", {title: 'Saving Data'});
                            $scope.isDataDirty = false;
                        });
                },
                function handleReject(error) {
                    // cancel
                }
            );
        };

        $scope.logout = function () {
            var promise = modals.open("confirm", {
                    message: "Are you sure you want to logout?"
                }
            );

            promise.then(
                function handleResolve(response) {
                    $state.go("logout");
                }
            );
        };

        $scope.export = function () {
            var data = angular.toJson($scope.organization, 4);
            var filename = "export.json"
            var blob = new Blob([data], {type: 'text/json'}),
                e = document.createEvent('MouseEvents'),
                a = document.createElement('a');

            a.download = filename;
            a.href = window.URL.createObjectURL(blob);
            a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
            e.initMouseEvent('click', true, false, window,
                0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
        };

        $scope.import = function () {
            var promise = modals.open("promptImport", {
                promptTitle: "Import Data"
            });

            promise.then(function handleResolve(response) {
                organizationFactory.loadFromData(response);
                dataLoaded();
            });
        };

        $scope.capture = function () {
            html2canvas(document.body, {
                onrendered: function(canvas) {
                    var a = document.createElement('a');
                    a.href = canvas.toDataURL("image/jpeg", 1.0);
                    a.download = $.now()+'.jpg';
                    a.click();
                }
            });
        };

        $scope.toggleActions = function () {
            $scope.isActionsVisible = !$scope.isActionsVisible;
        };

        $scope.getSeatTotal = function () {
            var total = 0;
            _.each($scope.organization.units, function (unit) {
                total += unit.seat;
            });
            return total;
        };

        $scope.getAreaTotal = function () {
            var total = 0;
            _.each($scope.organization.units, function (unit) {
                total += unit.area;
            });
            return total;
        };

        $scope.toggleBuildingSummaryVisibility = function () {
            $scope.isBuildingSummaryVisible = !$scope.isBuildingSummaryVisible;
        };

        $scope.toggleFooterVisibility = function () {
            $scope.isFooterVisible = !$scope.isFooterVisible;
        }
    }
}());