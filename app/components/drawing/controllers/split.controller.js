(function () {
    angular.module("stacker")
        .controller("SplitController", SplitController);

    SplitController.$inject = ["$scope", "organizationFactory"];

    function SplitController($scope, organizationFactory) {

        $scope.isValid = true;
        $scope.watcher = angular.noop;

        $scope.$on('splitUnit', function (event, args) {
            $scope.floor = args.floor;
            $scope.originalUnit = args.unit;
            $scope.isValid = true;

            $scope.unit1 = angular.copy($scope.originalUnit);
            $scope.unit1.area = Math.floor($scope.originalUnit.area / 2);
            $scope.unit1.seat = Math.floor($scope.originalUnit.seat / 2);

            $scope.unit2 = organizationFactory.createUnitFromExisting($scope.originalUnit);
            $scope.unit2.area = $scope.originalUnit.area - $scope.unit1.area;
            $scope.unit2.seat = $scope.originalUnit.seat - $scope.unit1.seat;

            $scope.watcher = $scope.$watch('unit1', function () {
                $scope.changeArea();
            }, true);
        });

        $scope.getUnitStyle = function (unit) {
            var availableWidth = 566 - 18;
            var width = Math.round((unit.area * availableWidth) / $scope.originalUnit.area);

            return {
                "width": width + "px",
                "background": "linear-gradient(to bottom,  " + unit.fill.startColor + " 0%," + unit.fill.startColor + " 16px," + unit.fill.endColor + " 16px," + unit.fill.endColor + " 100%)",
                "color": unit.font.color,
                "font-size": unit.font.fontSize,
                "float": "left"
            };
        };

        $scope.changeName = function (name) {
            $scope.isValid = !((typeof name == "undefined") || name == "")
        };

        $scope.changeCode = function (code) {
            $scope.isValid = !((typeof code == "undefined") || code == "")

            // check unique
            if ($scope.isValid) {
                if (organizationFactory.isUnit(code)) {
                    $scope.isValid = false;
                } else {
                    $scope.isValid = true;
                }
            }
        };

        $scope.changeArea = function () {
            // first check value
            if ((typeof $scope.unit1.area == "undefined") || $scope.unit1.area < 1 || $scope.unit1.area >= $scope.originalUnit.area) {
                $scope.isValid = false;
                $scope.unit2.area = "";
            } else {
                $scope.isValid = true;
                $scope.unit2.area = $scope.originalUnit.area - $scope.unit1.area;
                $scope.unit1.seat = Math.round(($scope.unit1.area * $scope.originalUnit.seat) / $scope.originalUnit.area);
                $scope.unit2.seat = $scope.originalUnit.seat - $scope.unit1.seat;
            }
        };

        $scope.changeSeat = function () {
            // first check value
            if ((typeof $scope.unit1.seat == "undefined") || $scope.unit1.seat < 1 || $scope.unit1.seat >= $scope.originalUnit.seat) {
                $scope.isValid = false;
                $scope.unit2.seat = "";
            } else {
                $scope.isValid = false;
                $scope.unit2.seat = $scope.originalUnit.seat - $scope.unit1.seat;
                $scope.unit1.area = Math.round(($scope.unit1.seat * $scope.originalUnit.area) / $scope.originalUnit.seat);
                $scope.unit2.area = $scope.originalUnit.area - $scope.unit1.area;
            }
        };

        $scope.save = function () {
            if (!$scope.isValid) {
                return;
            }

            var originalCode = $scope.originalUnit.code;
            $scope.floor = $scope.floor || organizationFactory.getFloorByUnitCode(originalCode);

            // update first unit
            angular.extend($scope.originalUnit, $scope.unit1);

            // update 2nd unit
            var index = _.findIndex(organizationFactory.org.units, function (u) {
                return (u.code == $scope.unit1.code);
            });
            if (index > -1) {
                organizationFactory.org.units.splice(index + 1, 0, $scope.unit2);
            }

            // update floor
            if ($scope.floor) {
                var floorIndex = $scope.floor.units.indexOf(originalCode);
                if (floorIndex > -1) {
                    $scope.floor.units[floorIndex] = $scope.unit1.code;
                    $scope.floor.units.splice(floorIndex + 1, 0, $scope.unit2.code);
                }
            }

            $scope.close();
        };

        $scope.close = function () {
            $scope.watcher();
            $scope.originalUnit = undefined;
            $scope.unit1 = undefined;
            $scope.unit2 = undefined;
        }

    }
}());