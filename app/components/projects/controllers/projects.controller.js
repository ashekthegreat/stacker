(function () {
    angular.module("stacker")
        .controller("ProjectsController", ProjectsController);

    ProjectsController.$inject = ["$scope", "$rootScope", "projectsFactory", "modals", 'growl'];

    function ProjectsController($scope, $rootScope, projectsFactory, modals, growl) {
        $scope.projects = [];
        //$scope.isLoading = true;
        $rootScope.pageTitle = "Projects";

        function loadData() {
            //$scope.isLoading = true;
            projectsFactory.loadProjects()
                .then(function (response) {
                    $scope.projects = response;
                    //$scope.isLoading = false;
                });
        }

        loadData();

        $scope.deleteProject = function (project) {
            var promise = modals.open("confirm", {
                    message: "Are you sure you want to delete this Project?"
                }
            );

            promise.then(
                function handleResolve(response) {
                    $scope.projects = _.reject($scope.projects, function (p) {
                        return p.id == project.id;
                    });
                    projectsFactory.deleteProject(project);
                }
            );
        };
        $scope.editProject = function (project) {
            var promise = modals.open("promptSaveProject", {
                    promptTitle: "Edit Project",
                    project: angular.copy(project)
                }
            );

            promise.then(
                function handleResolve(response) {
                    angular.extend(project, response);
                    projectsFactory.saveProject(project);
                }
            );
        };
        $scope.addProject = function () {
            var promise = modals.open("promptSaveProject", {
                    promptTitle: "New Project",
                    project: {
                        id: 0,
                        name: "",
                        description: "",
                        versions: []
                    }
                }
            );

            promise.then(
                function handleResolve(response) {
                    var project = response;
                    $scope.projects.push(project);
                    projectsFactory.saveProject(project).then(function (response) {
                        angular.extend(project, response.data);
                    });
                }
            );
        };

        $scope.exportVersion = function (id, versionId) {

            projectsFactory.loadProject(id, versionId)
                .then(function () {
                    var data = angular.toJson(projectsFactory.project.version.organization, 4);
                    var filename = projectsFactory.project.version.name + ".json"
                    var blob = new Blob([data], {type: 'text/json'}),
                        e = document.createEvent('MouseEvents'),
                        a = document.createElement('a');

                    a.download = filename;
                    a.href = window.URL.createObjectURL(blob);
                    a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
                    e.initMouseEvent('click', true, false, window,
                        0, 0, 0, 0, 0, false, false, false, false, 0, null);
                    a.dispatchEvent(e);
                });
        };
    }
}());