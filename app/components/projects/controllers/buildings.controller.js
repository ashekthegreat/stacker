(function () {
    angular.module("stacker")
        .controller("BuildingsController", BuildingsController);

    BuildingsController.$inject = ["$scope", "$state", "$stateParams", "modals", 'growl', "projectsFactory", "MEASUREMENT_UNITS"];

    function BuildingsController($scope, $state, $stateParams, modals, growl, projectsFactory, MEASUREMENT_UNITS) {
        //$scope.isLoading = true;

        $scope.measurementUnits = MEASUREMENT_UNITS; //_.pluck(MEASUREMENT_UNITS, 'title');
        $scope.project = {};
        $scope.pageTitle = ($stateParams.versionId ? "Edit Version" : "Import Version");

        if ($stateParams.projectId) {
            projectsFactory.loadProject($stateParams.projectId)
                .then(function (response) {
                    console.log(response);
                    $scope.project = response;
                    if ($stateParams.versionId) {
                        $scope.version = _.find($scope.project.versions, function (v) {
                            return v.id == $stateParams.versionId;
                        });
                    } else {
                        $state.go("project.info");
                    }
                    //$scope.isLoading = false;

                });
        } else {
            $state.go("projects");
        }

        function getBuilding(building) {
            return _.find($scope.version.organization.buildings, function (b) {
                return _.isEqual(b, building);
            });
        }

        function getFloor(building, floor) {
            return _.find(building.floors, function (f) {
                return _.isEqual(f, floor);
            });
        }

/*
        $scope.submitForm = function (createForm) {
            projectsFactory.saveVersion($scope.version).then(function () {
                growl.success("Saved successfully", {title: 'Saving Buildings'});
            });
        };
*/
        $scope.submitForm = function (isClose) {
            projectsFactory.saveVersion($scope.version).then(function () {
                growl.success("Data saved successfully", {title: 'Saving Buildings'});
                if(isClose){
                    //$state.go("project.info");
                } else{
                    $state.go("project.version.drawing");
                }
            });

        };

        $scope.addBuilding = function () {
            var promise = modals.open("promptBuilding", {
                    promptTitle: "Add Building",
                    building: {
                        title: "",
                        floors: []
                    }
                }
            );

            promise.then(
                function handleResolve(response) {
                    $scope.version.organization.buildings.push(response);
                }
            );
        };

        $scope.editBuilding = function (building) {
            var promise = modals.open("promptBuilding", {
                    promptTitle: "Edit Building",
                    building: angular.copy(building)
                }
            );

            promise.then(
                function handleResolve(response) {
                    angular.extend(building, response);
                }
            );
        };

        $scope.deleteBuilding = function (building) {
            modals.open("confirm", {
                    message: "Are you sure you want to delete this Building?"
                }
            ).then(
                function handleResolve(response) {
                    var org = $scope.version.organization;
                    org.buildings = _.reject(org.buildings, function (b) {
                        return _.isEqual(b, building);
                        //return b.title == building.title;
                    });
                }
            );
        };
        $scope.moveBuildingUp = function (index) {
            var buildings = $scope.version.organization.buildings;
            if (index > 0) {
                var temp = buildings[index];
                buildings[index] = buildings[index - 1];
                buildings[index - 1] = temp;
            }
        };
        $scope.moveBuildingDown = function (index) {
            var buildings = $scope.version.organization.buildings;
            if (index < buildings.length - 1) {
                var temp = buildings[index];
                buildings[index] = buildings[index + 1];
                buildings[index + 1] = temp;
            }
        };


        $scope.deleteFloor = function (floor, building) {
            modals.open("confirm", {
                    message: "Are you sure you want to delete this Floor?"
                }
            ).then(
                function handleResolve(response) {
                    building = getBuilding(building);
                    building.floors = _.reject(building.floors, function (f) {
                        return _.isEqual(f, floor);
                    });
                }
            );
        };
        $scope.addFloor = function (building) {
            building = getBuilding(building);
            var floor = {
                "title": "",
                "area": 0,
                "offset": 0,
                "void": 0,
                "core": 0,
                "units": []
            };

            var promise = modals.open("promptFloor", {
                    promptTitle: "Add Floor",
                    floor: floor,
                    measurementUnit: $scope.version.organization.measurementUnit
                }
            );

            promise.then(
                function handleResolve(response) {
                    building.floors.push(response);
                }
            );

        };
        $scope.editFloor = function (floor, building) {
            building = getBuilding(building);
            floor = getFloor(building, floor);

            var promise = modals.open("promptFloor", {
                    promptTitle: "Edit Floor",
                    floor: angular.copy(floor),
                    measurementUnit: $scope.version.organization.measurementUnit
                }
            );

            promise.then(
                function handleResolve(response) {
                    angular.extend(floor, response);
                }
            );

        };
        $scope.moveFloorUp = function (index, building) {
            if (index > 0) {
                building = getBuilding(building);
                var temp = building.floors[index];
                building.floors[index] = building.floors[index - 1];
                building.floors[index - 1] = temp;
            }
        };
        $scope.moveFloorDown = function (index, building) {
            if (index < building.floors.length - 1) {
                building = getBuilding(building);
                var temp = building.floors[index];
                building.floors[index] = building.floors[index + 1];
                building.floors[index + 1] = temp;
            }
        };
    }
}());