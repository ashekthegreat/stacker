(function () {
    angular.module("stacker")
        .controller("UnitsController", UnitsController);

    UnitsController.$inject = ["$scope", "$state", "$stateParams", "modals", "growl", "projectsFactory", "MEASUREMENT_UNITS", '$anchorScroll'];

    function UnitsController($scope, $state, $stateParams, modals, growl, projectsFactory, MEASUREMENT_UNITS, $anchorScroll) {
        //$scope.isLoading = true;

        $scope.measurementUnits = MEASUREMENT_UNITS; //_.pluck(MEASUREMENT_UNITS, 'title');
        $scope.project = {};
        $scope.pageTitle = "Organization Units";
        $scope.unit = {};
        $scope.isEditMode = false;
        $scope.originalUnit = {};
        $scope.setForm = function (createForm) {
            $scope.form = createForm;
        };

        if ($stateParams.projectId) {
            projectsFactory.loadProject($stateParams.projectId)
                .then(function (response) {
                    $scope.project = response;
                    if ($stateParams.versionId) {
                        $scope.version = _.find($scope.project.versions, function (v) {
                            return v.id == $stateParams.versionId;
                        });
                        $scope.organization = $scope.version.organization;
                    } else {
                        $state.go("project.info");
                    }
                    //$scope.isLoading = false;
                    $scope.resetForm();
                });
        } else {
            $state.go("projects");
        }


        $scope.resetForm = function () {
            $scope.unit = {
                "name": "",
                "code": "",
                "area": "", //0,
                "isShowArea": true,
                "seat": "", //0,
                "isShowSeat": false,
                "areaPerSeat": "", //0,
                "isUsed": false,
                "fill": {
                    "startColor": "#cccccc",
                    "endColor": "#999999"
                },
                "font": {
                    "color": "#ffffff",
                    "fontSize": "11px"
                }
            };
            $scope.isEditMode = false;
            $scope.originalUnit = {};
            if($scope.form){
                $scope.form.$setPristine();
                $scope.form.$setUntouched();
            }
        };


        $scope.editUnit = function (unit) {
            $scope.unit = angular.copy(unit);
            $scope.isEditMode = true;
            $scope.originalUnit = unit;
            $anchorScroll.yOffset = 110;
            $anchorScroll('createForm');
        };

        $scope.deleteUnit = function (unit) {
            var promise = modals.open("confirm", {
                    message: "Are you sure you want to delete this Unit?"
                }
            );

            promise.then(
                function handleResolve(response) {
                    $scope.resetForm();
                    // delete any usage of unit at any floor
                    _.each($scope.organization.buildings, function (building) {
                        _.each(building.floors, function (floor) {
                            floor.units = _.reject(floor.units, function (u) {
                                return u == unit.code;
                            });
                        });
                    });

                    // delete unit itself
                    $scope.organization.units = _.reject($scope.organization.units, function (u) {
                        return u.code == unit.code;
                    });
                },
                function handleReject(error) {
                    // cancel
                }
            );
        };

        $scope.saveUnit = function () {
            if($scope.isEditMode){
                var originalCode = $scope.originalUnit.code;
                angular.extend($scope.originalUnit, $scope.unit);

                if (isNaN($scope.originalUnit.seat)) {
                    $scope.originalUnit.seat = "";
                }

                // update floor
                _.each($scope.organization.buildings, function (building) {
                    _.each(building.floors, function (floor) {
                        var floorIndex = floor.units.indexOf(originalCode);
                        if (floorIndex > -1) {
                            floor.units[floorIndex] = $scope.unit.code;
                        }
                    });
                });
            } else{
                $scope.organization.units.push(angular.copy($scope.unit));
            }
            $scope.resetForm();
        };

        $scope.getSeatTotal = function () {
            var total = 0;
            _.each($scope.organization.units, function (unit) {
                total += unit.seat;
            });
            return total;
        };

        $scope.getAreaTotal = function () {
            var total = 0;
            _.each($scope.organization.units, function (unit) {
                total += unit.area;
            });
            return total;
        };

        $scope.getUnitStyle = function (unit) {
            if(!unit || !unit.fill) {
                return {};
            }

            return {
                "background": "linear-gradient(to bottom,  " + unit.fill.startColor + " 0%," + unit.fill.startColor + " 16px," + unit.fill.endColor + " 16px," + unit.fill.endColor + " 100%)",
                "cursor": "default"
            };
        };

        $scope.submitForm = function (isClose) {
            projectsFactory.saveVersion($scope.version).then(function () {
                growl.success("Data saved successfully", {title: 'Saving Data'});
                if(isClose){
                    $state.go("project.info");
                } else{
                    $state.go("project.version.buildings");
                }
            });

        };
    }
}());