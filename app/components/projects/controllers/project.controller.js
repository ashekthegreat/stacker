(function () {
    angular.module("stacker")
        .controller("ProjectController", ProjectController);

    ProjectController.$inject = ["$scope", "$rootScope", "$state", "$stateParams", "projectsFactory", "modals", 'growl'];

    function ProjectController($scope, $rootScope, $state, $stateParams, projectsFactory, modals, growl) {
        console.log("ProjectController");
        $scope.project = [];
        //$scope.isLoading = true;
        $rootScope.pageTitle = "";
        $rootScope.projectId = $stateParams.projectId;

        function loadData() {
            //$scope.isLoading = true;
            if($stateParams.projectId){
                projectsFactory.loadProject($stateParams.projectId)
                    .then(function (response) {
                        $scope.project = response;
                        //$scope.isLoading = false;
                        $rootScope.pageTitle = response.name;
                    });
            } else{
                $scope.project = {
                    id: 0,
                    name: "",
                    description: "",
                    contact: "",
                    logo: ""
                };
                //$scope.isLoading = false;
                $rootScope.pageTitle = "New Project";
            }
        }

        loadData();

        $scope.deleteVersion = function (version) {
            var promise = modals.open("confirm", {
                message: "Are you sure you want to delete this Version?"
            });

            promise.then(function handleResolve(response) {
                $scope.project.versions = _.reject($scope.project.versions, function (v) {
                    return v.id == version.id;
                });
                projectsFactory.deleteVersion(version);
            });
        };
        $scope.editProject = function (project) {
            var promise = modals.open("promptSaveProject", {
                promptTitle: "Edit Project",
                project: angular.copy(project)
            });

            promise.then(function handleResolve(response) {
                angular.extend(project, response);
                projectsFactory.saveProject(project);
            });
        };
        $scope.addProject = function () {
            var promise = modals.open("promptSaveProject", {
                promptTitle: "New Project",
                project: {
                    id: 0,
                    name: "",
                    description: ""
                }
            });

            promise.then(function handleResolve(response) {
                var project = response;
                $scope.projects.push(project);
                projectsFactory.saveProject(project).then(function (response) {
                    angular.extend(project, response.data);
                });
            });
        };

        $scope.submitForm = function (createForm) {
            projectsFactory.saveProject($scope.project).then(function (response) {
                angular.extend($scope.project, response.data);
                //$state.go("project.version.crud", {projectId: $scope.project.id});
            });

        };

        $scope.moveNext = function (isCreateVersion) {
            if(isCreateVersion){
                if($scope.project.id){
                    $state.go("project.version.crud", {projectId: $scope.project.id});
                }
            } else{
                $state.go("projects");
            }
        };

        $scope.exportVersion = function (version) {
            var data = angular.toJson(version.organization, 4);
            var filename = $scope.sanitizeFileName($scope.project.name + "-" + version.name + ".json", "_");
            var blob = new Blob([data], {type: 'text/json'}),
                e = document.createEvent('MouseEvents'),
                a = document.createElement('a');

            a.download = filename;
            a.href = window.URL.createObjectURL(blob);
            a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
            e.initMouseEvent('click', true, false, window,
                0, 0, 0, 0, 0, false, false, false, false, 0, null);
            a.dispatchEvent(e);
        };

        $scope.sanitizeFileName = function(input, replacement){
            var illegalRe = /[\/\?<>\\:\*\|":]/g;
            var controlRe = /[\x00-\x1f\x80-\x9f]/g;
            var reservedRe = /^\.+$/;
            var windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
            var spaces = /\s+/g;

            var sanitized = input
                .replace(illegalRe, replacement)
                .replace(controlRe, replacement)
                .replace(reservedRe, replacement)
                .replace(windowsReservedRe, replacement)
                .replace(spaces, replacement);
            return sanitized.toLowerCase();
        };

        $scope.fileChanged = function (files) {
            var file = files[0];

            projectsFactory.uploadLogo(file, $scope.project);
        };
    }
}());