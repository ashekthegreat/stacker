(function () {
    angular.module("stacker")
        .factory("projectsFactory", projectsFactory);

    projectsFactory.$inject = ["$http", "$q", "$window", "XLSXReaderService", "Upload"];

    function projectsFactory($http, $q, $window, XLSXReaderService, Upload) {
        var factory = {};

        factory.loadProjects = function () {
            return $http.get('backend/load_projects.php').then(function (payload) {
                return payload.data;
            });
        };

        factory.loadProjectsBasic = function () {
            return $http.get('backend/load_projects_basic.php').then(function (payload) {
                return payload.data;
            });
        };

        factory.loadProject = function (id) {
            return $http.get('backend/load_project.php?id=' + id).then(function (payload) {
                _.each(payload.data.versions, function (v) {
                    v.organization = JSON.parse(v.organization);
                });
                return payload.data;
            });
        };

        factory.loadProjectBasic = function (id) {
            return $http.get('backend/load_project_basic.php?id=' + id).then(function (payload) {
                return payload.data;
            });
        };

        factory.saveProject = function (project) {
            return $http.post('backend/save_project.php', project).success(function (data, status, headers, config) {
                // saving successful
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

        };

        factory.deleteProject = function (project) {
            return $http.post('backend/delete_project.php', project).success(function (data, status, headers, config) {
                // saving successful
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

        };

        factory.saveVersion = function (version) {
            return $http.post('backend/save_version.php', version).success(function (data, status, headers, config) {
                // saving successful
            }).error(function (data, status, headers, config) {
                console.log(data);
            });
        };

        factory.deleteVersion = function (version) {
            return $http.post('backend/delete_version.php', version).success(function (data, status, headers, config) {
                // saving successful
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

        };

        /*factory.deleteProject = function (project, version) {
         var id = project.id;
         var versionId = version.id;

         project.versions = _.reject(project.versions, function (v) {
         return v.id == versionId;
         });
         if (project.versions.length <= 0) {
         factory.projects = _.reject(factory.projects, function (p) {
         return p.id == id;
         });
         }

         return $http.get('backend/delete_project.php?id=' + id + '&version_id=' + versionId).then(function (payload) {
         // do nothing
         });
         };*/

        factory.handleJson = function (file) {
            var deferred = $q.defer();
            var reader = new FileReader();
            reader.onload = function (evt) {
                var org = JSON.parse(evt.target.result);
                deferred.resolve(org);
            };
            reader.readAsText(file);
            return deferred.promise;
        };

        factory.handleExcel = function (file) {
            return XLSXReaderService.readFile(file, true).then(function (data) {
                var organization = {
                    buildings: [],
                    units: []
                };

                // process units first
                _.each(data.sheets, function (sheet, sheetName) {
                    if (sheetName == "Units") {
                        organization.units = processUnits(sheet);
                    } else {
                        organization.buildings.push(processBuilding(sheet, sheetName));
                    }
                });
                return organization;
            });
        };

        factory.uploadLogo = function (file, project) {
            Upload.upload({
                url: 'backend/save_logo.php',
                data: {file: file, 'project_id': project.id}
            }).then(function (resp) {
                console.log(resp.data);
                project.logo = resp.data;
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        };

        function processUnits(sheet) {
            var units = [];
            var u;
            for (var i = 1; i < sheet.data.length; i++) {
                u = sheet.data[i];
                if (!_.isUndefined(u[0]) && !_.isUndefined(u[1]) && !_.isUndefined(u[2])) {
                    units.push({
                        "name": u[0],
                        "code": u[1],
                        "area": +u[2] || 0,
                        "isShowArea": !!u[4],
                        "seat": +u[3] || 0,
                        "isShowSeat": !!u[5],
                        "areaPerSeat": +(u[2] / u[3]) || 0,
                        "isUsed": false,
                        "fill": {
                            "startColor": u[6],
                            "endColor": u[7]
                        },
                        "font": {
                            "color": u[8],
                            "fontSize": u[9]
                        }
                    });
                }
            }
            return units;
        }

        function processBuilding(sheet, sheetName) {
            var building = {
                title: sheetName,
                floors: []
            };
            var u;
            for (var i = 1; i < sheet.data.length; i++) {
                u = sheet.data[i];
                if (!_.isUndefined(u[0]) && !_.isUndefined(u[1])) {
                    // its a valid floor
                    building.floors.push({
                        "title": u[0],
                        "area": +u[1] || 0,
                        "offset": +u[2] || 0,
                        "void": +u[3] || 0,
                        "core": +u[4] || 0,
                        "units": []
                    })
                }
            }
            return building;
        }

        return factory;
    }
}());