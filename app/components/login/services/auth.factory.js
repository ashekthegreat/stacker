(function () {
    angular.module("stacker")
        .service("authFactory", authFactory);

    authFactory.$inject = ["$http", "$q", "$window", "store", "$state", "$stateParams", "$location"];

    function authFactory($http, $q, $window, store, $state, $stateParams, $location) {
        var factory = {};

        factory.toStateParams = {};

        factory.isLoggedIn = function () {
            var auth = factory.getLogin();
            return (auth && auth.token);
        };

        factory.checkAccess = function (state, stateParams) {
            factory.toStateParams = stateParams;

            // look for safe states that will always have access
            var safeStates = [
                "logout",
                "login"/*,
                "drawing",
                "drawingWithoutVersion",
                "drawingWithoutVersionAndProject"*/
            ];

            if (safeStates.indexOf(state.name) > -1) {
                return true;
            }

            var auth = factory.getLogin();
            if (auth) {
                if (auth.type == "admin") {
                    return true;
                }

                if (state.name == "drawing") {
                    if (stateParams.projectId && (auth.projectIds.indexOf(stateParams.projectId) > -1)) {
                        return true;
                    }
                } else {
                    if (auth.type == "admin") {
                        return true;
                    }
                }
            }
            factory.logout();
        };

        factory.setLogin = function (auth) {
            /*var auth = {
             projectId: 0,
             token: 'token string'
             };*/
            store.set("auth", auth);
        };
        factory.getLogin = function () {
            return store.get("auth");
        };
        factory.getToken = function () {
            return factory.getLogin().token;
        };
        factory.removeLogin = function () {
            store.remove("auth");
        };
        factory.logout = function () {
            factory.removeLogin();
            console.log("logging out then");
            $state.go("login");
        };

        factory.login = function (user) {
            factory.removeLogin();

            return $http.post('backend/login.php', user).success(function (data, status, headers, config) {
                // saving successful
                if (data.success) {
                    factory.setLogin(data.auth);
                    if (data.auth.type == "admin") {
                        $state.go("projects");
                    } else {
                        $state.go("projects", {
                            projectId: data.auth.projectId,
                            versionId: (factory.toStateParams.versionId || 1)
                        })
                    }
                } else {
                    factory.removeLogin();
                }

            }).error(function (data, status, headers, config) {
                factory.removeLogin();
            });
        };

        factory.getLoginUrl = function () {
            return $location.absUrl().replace($location.url(), $state.get("login").url);
        };

        return factory;
    }
}());