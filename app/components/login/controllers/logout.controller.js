(function () {
    angular.module("stacker")
        .controller("LogoutController", LogoutController);

    LogoutController.$inject = ["$scope", "$state", "$stateParams", "projectsFactory", "authFactory"];

    function LogoutController($scope, $state, $stateParams, projectsFactory, authFactory) {
        authFactory.logout();
    }

}());