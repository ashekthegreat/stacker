(function () {
    angular.module("stacker")
        .controller("LoginController", LoginController);

    LoginController.$inject = ["$scope", "$state", "$stateParams", "projectsFactory", "authFactory"];

    function LoginController($scope, $state, $stateParams, projectsFactory, authFactory) {
        if (authFactory.isLoggedIn()) {
            authFactory.logout();
        }
        console.log($stateParams);
        $scope.type = "admin"; // project/admin
        $scope.project = {
            id: 0,
            name: "&nbsp;",
            logo: 'logo.png'
        };
        $scope.user = {
            email: "",
            password: ""
        };

        if ($stateParams.projectId) {
            $scope.type = "project";

            projectsFactory.loadProjectBasic($stateParams.projectId).then(function (project) {
                $scope.project = project;
            })
        } else {
            $scope.type = "admin";
            // load admin related info
        }

        $scope.checkLogin = function (login) {
            $scope.message = "";
            authFactory.login($scope.user).then(function (payload) {
                if (!payload.data.success) {
                    $scope.message = payload.data.message;
                }
            });
        }
    }
}());