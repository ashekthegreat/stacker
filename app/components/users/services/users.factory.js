(function () {
    angular.module("stacker")
        .factory("usersFactory", usersFactory);

    usersFactory.$inject = ["$http", "$q", "$window"];

    function usersFactory($http, $q, $window) {
        var factory = {};

        factory.loadUsers = function () {
            return $http.get('backend/load_users.php').then(function (payload) {

                return payload.data;
            });
        };

        factory.loadUser = function (id) {
            return $http.get('backend/load_user.php?id=' + id).then(function (payload) {
                _.each(payload.data.versions, function (v) {
                    v.organization = JSON.parse(v.organization);
                });
                return payload.data;
            });
        };

        factory.saveUser = function (user) {
            return $http.post('backend/save_user.php', user).success(function (data, status, headers, config) {
                // saving successful
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

        };

        factory.deleteUser = function (user) {
            return $http.post('backend/delete_user.php', user).success(function (data, status, headers, config) {
                // saving successful
            }).error(function (data, status, headers, config) {
                console.log(data);
            });

        };

        return factory;

    }
}());