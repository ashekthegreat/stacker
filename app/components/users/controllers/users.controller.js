(function () {
    angular.module("stacker")
        .controller("UsersController", UsersController);

    UsersController.$inject = ["$scope", "$rootScope", "usersFactory", "modals", 'growl', "projects"];

    function UsersController($scope, $rootScope, usersFactory, modals, growl, projects) {
        $scope.projects = projects;
        $scope.users = [];
        //$scope.isLoading = true;
        $rootScope.pageTitle = "Users";

        function loadData() {
            //$scope.isLoading = true;
            usersFactory.loadUsers()
                .then(function (response) {
                    $scope.users = response;
                    //$scope.isLoading = false;
                });
        }

        loadData();

        $scope.deleteUser = function (user) {
            var promise = modals.open("confirm", {
                message: "Are you sure you want to delete this User?"
            });

            promise.then(function handleResolve(response) {
                $scope.users = _.reject($scope.users, function (u) {
                    return u.id == user.id;
                });
                usersFactory.deleteUser(user);
            });
        };
        $scope.editUser = function (user) {
            var promise = modals.open("promptSaveUser", {
                promptTitle: "Edit User",
                user: angular.copy(user),
                projects: $scope.projects
            });

            promise.then(function handleResolve(response) {
                //console.log(response);
                angular.extend(user, response);
                usersFactory.saveUser(user).then(function (response) {
                    user.password = "";
                });
            });
        };
        $scope.addUser = function () {
            var promise = modals.open("promptSaveUser", {
                promptTitle: "New User",
                user: {
                    id: 0,
                    name: "",
                    email: "",
                    password: "",
                    type: "admin",
                    projectId: 0,
                    note: ""
                },
                projects: $scope.projects
            });

            promise.then(function handleResolve(response) {
                var user = response;
                $scope.users.push(user);
                usersFactory.saveUser(user).then(function (response) {
                    angular.extend(user, response.data);
                    user.password = "";
                });
            });
        };
    }
}());