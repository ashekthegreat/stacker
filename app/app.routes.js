/* global angular: false */

(function (ng) {
    'use strict';

    //$stateprovider is the service provided by ui.router
    angular.module("stacker")
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

            //create route object
            $stateProvider
                .state('projects', {
                    url: '/',
                    templateUrl: 'app/components/projects/views/index.html',
                    controller: 'ProjectsController',
                    showNav: true
                })
                .state('project', {
                    abstract: true,
                    url: '/project/:projectId',
                    templateUrl: 'app/components/projects/views/project_base.html',
                    showNav: true
                })
                .state('project.info', {
                    url: '',
                    templateUrl: 'app/components/projects/views/project.html',
                    controller: 'ProjectController',
                    showNav: true
                })
                .state('project.version', {
                    abstract: true,
                    url: '/version/:versionId',
                    templateUrl: 'app/components/projects/views/version_base.html',
                    showNav: true,
                    resolve: {
                        project:  function($stateParams, projectsFactory){
                            return projectsFactory.loadProject($stateParams.projectId)
                                .then(function (response) {
                                    return response;
                                });
                        }
                    },
                    controller: function($scope, project){
                        $scope.project = project;
                    }
                })
                .state('project.version.crud', {
                    url: '/crud',
                    templateUrl: 'app/components/projects/views/version.html',
                    controller: 'VersionController',
                    showNav: true
                })
                .state('project.version.units', {
                    url: '/units',
                    templateUrl: 'app/components/projects/views/units.html',
                    controller: 'UnitsController',
                    showNav: true
                })
                .state('project.version.buildings', {
                    url: '/buildings',
                    templateUrl: 'app/components/projects/views/buildings.html',
                    controller: 'BuildingsController',
                    showNav: true
                })
                .state('project.version.drawing', {
                    url: '/drawing',
                    templateUrl: 'app/components/drawing/views/index.html',
                    controller: 'DrawingController',
                    showNav: false/*,
                     resolve: {
                     projects:  ['projectsFactory', function(projectsFactory){
                     return projectsFactory.loadProjects();
                     }]
                     }*/
                })

                .state('users', {
                    url: '/users',
                    templateUrl: 'app/components/users/views/index.html',
                    controller: 'UsersController',
                    showNav: true,
                    resolve: {
                        projects:  ['projectsFactory', function(projectsFactory){
                            return projectsFactory.loadProjectsBasic();
                        }]
                    }
                })
                .state('login', {
                    url: '/login',
                    templateUrl: 'app/components/login/views/index.html',
                    controller: 'LoginController'
                })
                .state('logout', {
                    url: '/logout',
                    template: 'Logging Out',
                    controller: 'LogoutController'
                })

                .state('drawingWithoutVersion', {
                    url: '/drawing/:projectId',
                    templateUrl: 'app/components/drawing/views/index.html',
                    controller: 'DrawingController'/*,
                     resolve: {
                     projects:  ['projectsFactory', function(projectsFactory){
                     return projectsFactory.loadProjects();
                     }]
                     }*/
                })
                .state('drawingWithoutVersionAndProject', {
                    url: '/drawing',
                    templateUrl: 'app/components/drawing/views/index.html',
                    controller: 'DrawingController'/*,
                     resolve: {
                     projects:  ['projectsFactory', function(projectsFactory){
                     return projectsFactory.loadProjects();
                     }]
                     }*/
                });

            // set the default route
            $urlRouterProvider.otherwise('/');

        }]);
})(angular);
