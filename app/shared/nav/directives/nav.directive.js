angular.module('stacker')
    .directive('stNav', function () {
        return {
            restrict: 'EA', //E = element, A = attribute, C = class, M = comment
            scope: {
                pageTitle: "=",
                projectId: "=",
                versionId: "="
            },
            templateUrl: 'app/shared/nav/views/index.html',
            replace: false,
            controller: ['$scope', '$http', 'authFactory', '$timeout', function ($scope, $http, authFactory, $timeout) {
                $scope.auth = authFactory.getLogin();
                $scope.isDropdownOpen = false;

                $scope.toggleDropdown = function (toggleState) {
                    $timeout(function () {
                        $scope.isDropdownOpen = toggleState;
                    }, (toggleState?0:500));
                }
            }],
            link: function ($scope, element, attrs) {

            }
        }
    });