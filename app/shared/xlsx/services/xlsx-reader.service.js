(function(undefined) {
    // Get angular app
    angular.module("stacker").factory("XLSXReaderService", ['$q', '$rootScope',
        function($q, $rootScope) {
            var service = function(data) {
                angular.extend(this, data);
            };

            service.readFile = function(file, showPreview) {
                var deferred = $q.defer();

                // file, readCells, toJSON, handler
                XLSXReader(file, showPreview, false, function(data){
                    $rootScope.$apply(function() {
                        deferred.resolve(data);
                    });
                });

                return deferred.promise;
            };

            return service;
        }
    ]);
}).call(this);
