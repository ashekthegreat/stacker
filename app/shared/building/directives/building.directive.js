angular.module('stacker')
    .directive('stBuilding', function () {
        return {
            restrict: 'EA', //E = element, A = attribute, C = class, M = comment
            scope: {
                //@ reads the attribute value, = provides two-way binding, & works with functions
                building: '=',
                organization: '='
            },
            templateUrl: 'app/shared/building/views/index.html',
            replace: true,
            controller: ['$scope', '$http', "organizationFactory", function ($scope, $http, organizationFactory) {
                $scope.getFloorStyle = function (floor) {
                    return {
                        width: ((floor.area * $scope.ratio) + 2) + "px",
                        left: (floor.offset * $scope.ratio) + "px"
                    };
                };
                $scope.getFloorUnits = function (floor) {
                    var floorUnits = [];

                    _.each(floor.units, function (unitCode) {
                        floorUnits.push(_.find($scope.organization.units, function (unit, i) {
                            return unit && unit.code == unitCode;
                        }));
                    });

                    return floorUnits;
                };
                $scope.getUnitStyle = function (unit, floor) {
                    var style = {};
                    if (unit == "void") {
                        style = {
                            width: (floor.void * $scope.ratio) + "px"
                        }
                    } else if (unit == "core") {
                        style = {
                            width: (floor.core * $scope.ratio) + "px"
                        }
                    } else {
                        style = {
                            "width": (unit.area * $scope.ratio) + "px",
                            "background": "linear-gradient(to bottom,  " + unit.fill.startColor + " 0%," + unit.fill.startColor + " 30%," + unit.fill.endColor + " 30%," + unit.fill.endColor + " 100%)",
                            "color": unit.font.color,
                            "font-size": unit.font.fontSize
                        };
                    }
                    return style;
                };
                $scope.getFloorUnitContainerStyle = function (floor) {
                    var floorUnits = $scope.getFloorUnits(floor);
                    var w = 0 + floor.void + floor.core;
                    _.each(floorUnits, function (unit) {
                        w += unit.area;
                    });

                    return {
                        width: ((w * $scope.ratio) + 4) + "px"
                    };
                };
                $scope.remainingFloorArea = function (floor) {
                    return organizationFactory.remainingFloorArea(floor);
                };

            }],
            link: function ($scope, element, attrs) {
                $scope.width = $(element).width() - 10;
                $scope.labelWidth = $(element).find(".th.area-label").outerWidth(true) + $(element).find(".th.floor-label").outerWidth(true);
                var maxFloorArea = 0;
                if (!_.isEmpty($scope.building)) {
                    _.each($scope.building.floors, function (floor) {
                        maxFloorArea = (floor.area > maxFloorArea) ? floor.area : maxFloorArea;
                    });
                    $scope.ratio = ($scope.width - $scope.labelWidth) / maxFloorArea;
                }
                setTimeout(function(){
                    //console.log($(element));
                    //console.log($(element).closest(".building-snapshot"));
                    $(element).closest(".building-snapshot").stick_in_parent({
                        parent: $(element).closest(".row")
                    });
                },0);
                setTimeout(function(){
                    $(".building-list").find(".row").each(function(){
                        var maxHeight = 0;
                        $(this).find("> *").each(function(){
                            if($(this).height() > maxHeight){
                                maxHeight = $(this).height();
                            }
                        });
                        $(this).find("> *").css("min-height", maxHeight + "px");
                    });
                },100);


                //console.log(scope.building);
                //console.log(scope.organization);
            } //DOM manipulation
        }
    });