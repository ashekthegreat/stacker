(function () {
    angular.module("stacker")
        .constant("MEASUREMENT_UNITS", [
            {
                title: "sft",
                factor: 1
            },
            {
                title: "sqm",
                factor: 0.1
            },
            {
                title: "坪 (peng)",
                factor: 0.027777777777
            }
        ])
}());