angular.module('stacker')
    .directive('stFullHeight', function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment
            link: function ($scope, element, attrs) {
                var $item = $(element);
                if($item.length){
                    $item.css('min-height', $item.parent().height());
                }
            }
        }
    });