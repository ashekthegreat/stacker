(function () {
    var app = angular.module("stacker");

    // This makes any element draggable
    // Usage: <div draggable>Foobar</div>
    app.directive('draggable', function () {
        return {
            // A = attribute, E = Element, C = Class and M = HTML Comment
            restrict: 'A',
            //The link function is responsible for registering DOM listeners as well as updating the DOM.
            link: function (scope, element, attrs) {
                element.draggable({
                    revert: "invalid",
                    appendTo: "body",
                    refreshPositions: true,
                    connectToSortable: ".unit-container"
                });
                element.disableSelection();
            }
        };
    });

    // This makes any element droppable
    // Usage: <div droppable></div>
    app.directive('droppable', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                //This makes an element Droppable
                element.droppable({
                    drop: function (event, ui) {
                        var dragIndex = angular.element(ui.draggable).data('index'),
                            reject = angular.element(ui.draggable).data('reject'),
                            dragEl = angular.element(ui.draggable).parent(),
                            dropEl = angular.element(this);

                        if (dragEl.hasClass('list1') && !dropEl.hasClass('list1') && reject !== true) {
                            scope.list2.push(scope.list1[dragIndex]);
                            scope.list1.splice(dragIndex, 1);
                        } else if (dragEl.hasClass('list2') && !dropEl.hasClass('list2') && reject !== true) {
                            scope.list1.push(scope.list2[dragIndex]);
                            scope.list2.splice(dragIndex, 1);
                        }
                        scope.$apply();
                    }
                });
            }
        };
    });

    // This makes any element sortable
    // Usage: <div sortable></div>
    app.directive('sortable', function ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {

                scope.$watch(function (scope) {
                        return scope.mergeUnits.unit1
                    },
                    function (newValue, oldValue) {
                        element.sortable(newValue ? "disable" : "enable");
                    }
                );
                element.sortable({
                    items: ".unit:not(.unit-void,.unit-core)",
                    tolerance: "pointer",
                    appendTo: $(".drawing-canvas"),
                    helper: "clone",
                    connectWith: ".unit-container, .org-units",
                    scroll: false,
                    revert: "invalid",
                    containment: "",
                    start: function(event, ui){
                        setTimeout(function(){
                            ui.placeholder.width(ui.helper.width());
                        },10);
                    },
                    over: function( event, ui ) {
                        $(event.target).closest(".floor-space").addClass("drag-over");
                    },
                    out: function( event, ui ) {
                        $(event.target).closest(".floor-space").removeClass("drag-over");
                    },
                    update: function( event, ui ) {
                        var $item = $(ui.item);
                        var $target = $(event.target);

                        if($target.hasClass("unit-container")){
                            // floors
                            var targetUnitCodes = [];
                            $target.find(".unit:not(.unit-void)").each(function () {
                                if ($(this).data("code")) {
                                    targetUnitCodes.push($(this).data("code"));
                                }
                            });
                            scope.updateFloorUnits($target.data("floor-title"), targetUnitCodes);
                        }
                        setTimeout(function(){
                            scope.$apply();
                        }, 500);
                        //scope.$apply();
                    },
                    receive: function (event, ui) {
                        var $item = $(ui.item);
                        var $target = $(event.target);

                        if($target.hasClass("org-units") ){
                            // item reverted back
                            scope.changeUnitProperty($item.data("code"), "isUsed", false);
                            scope.insertUnitAfter($item.data("code"), $item.prev('.unit').data("code"));

                        } else if($target.hasClass("unit-container")){
                            scope.changeUnitProperty($item.data("code"), "isUsed", true);
                            // floors
                            var targetUnitCodes = [];
                            $target.find(".unit:not(.unit-void)").each(function () {
                                if ($(this).data("code")) {
                                    targetUnitCodes.push($(this).data("code"));
                                }
                            });
                            scope.updateFloorUnits($target.data("floor-title"), targetUnitCodes);
                        }

                        $item.remove();
                        scope.$apply();
                    },
                    remove: function (event, ui) {
                        var $item = $(ui.item);
                        var $target = $(event.target);
                        if($target.hasClass("org-units") ){
                            // item reverted back
                            scope.changeUnitProperty($item.data("code"), "isUsed", true);
                        }
                    }
                });
                element.disableSelection();
            }
        };
    });
}());