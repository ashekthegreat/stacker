<?php
/**
 * Created by PhpStorm.
 * User: AZEC
 * Date: 7/26/2015
 * Time: 3:35 AM
 */

require_once 'medoo.min.php';

$database = new medoo();

$id = $_GET['id'];

// lets get the project
$project = $database->get("projects", [
    "id",
    "name",
    "description",
    "created_at"
], [
    "id" => $id
]);

// lets grab the version. If not specific, grab the first one
if (isset($_GET['version_id'])) {
    $version_id = $_GET['version_id'];
    $project['version'] = $database->get("versions", "*", [
        "AND" => [
            "id" => $version_id,
            "project_id" => $id
        ]
    ]);
} else {
    $project['version'] = $database->get("versions", "*", [
        "project_id" => $id
    ]);
}

echo json_encode($project);