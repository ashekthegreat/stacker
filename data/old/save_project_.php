<?php
/**
 * Created by PhpStorm.
 * User: AZEC
 * Date: 7/26/2015
 * Time: 2:29 AM
 */
require_once 'medoo.min.php';

$database = new medoo();

/*
* Collect all Details from Angular HTTP Request.
*/
$postData = file_get_contents("php://input");
$request = json_decode($postData);

$project_id = $request->id;
$version_id = $request->version->id;

if($request->id){
    // its an existing project
    $database->update("projects", [
        "name" => $request->name,
        "description" => $request->description
    ], [
        "id" => $request->id
    ]);

    $database->update("versions", [
        "name" => $request->version->name,
        "note" => $request->version->note,
        "organization" => json_encode($request->version->organization)
    ], [
        "id" => $request->version->id
    ]);

}else{
    $project_id = $database->insert("projects", [
        "name" => $request->name,
        "description" => $request->description
    ]);

    $version_id = $database->insert("versions", [
        "project_id" => $project_id,
        "name" => $request->version->name,
        "note" => $request->version->note,
        "organization" => json_encode($request->version->organization)
    ]);
}


echo json_encode(array("id"=>$project_id, "versionId"=>$version_id));