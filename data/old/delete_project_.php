<?php
/**
 * Created by PhpStorm.
 * User: AZEC
 * Date: 7/26/2015
 * Time: 4:03 AM
 */
require_once 'medoo.min.php';

$database = new medoo();

$id = $_GET['id'];
$version_id = 0;

if (isset($_GET['version_id'])) {
    $version_id = $_GET['version_id'];
}

$versions = $database->delete("versions", [
    "AND" => [
        "project_id" => $id,
        "id" => $version_id
    ]
]);
$versions = $database->get("versions", "*", [
    "project_id" => $id
]);
if($versions == false){
    $organization = $database->delete("projects", [
        "AND" => [
            "id" => $id
        ]
    ]);
}

echo "success";