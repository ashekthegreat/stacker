-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2016 at 09:07 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stacker`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contact` text NOT NULL,
  `logo` text
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `created_at`, `contact`, `logo`) VALUES
(1, 'Sample project', 'Building and unit distribution of Sample Organization', '2015-07-21 18:30:08', '', NULL),
(11, 'Google Inc', 'Project for Google space distribution', '2015-07-25 22:27:30', '', NULL),
(16, 'Twitter', 'Twitter Head office', '2015-07-26 15:00:46', '', NULL),
(22, 'Facebook', 'Facebook Head office', '2015-09-22 08:44:28', '', NULL),
(23, 'hghf', 'fhgf', '2015-09-22 08:45:50', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'admin',
  `project_ids` text NOT NULL,
  `note` text
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `project_ids`, `note`) VALUES
(1, 'Admin', 'admin@stacker.com', '827ccb0eea8a706c4c34a16891f84e7b', 'admin', '[]', 'Stacker admin. Allowed at all sections'),
(2, 'ashek', 'ashek@stacker.com', '827ccb0eea8a706c4c34a16891f84e7b', 'project', '["11"]', 'Only allowed to Sample Project (id: 1)'),
(4, 'David', 'david@stacker.com', '827ccb0eea8a706c4c34a16891f84e7b', 'project', '["1"]', 'Sample project employee');

-- --------------------------------------------------------

--
-- Table structure for table `versions`
--

CREATE TABLE IF NOT EXISTS `versions` (
  `id` bigint(20) NOT NULL,
  `project_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `organization` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `versions`
--

INSERT INTO `versions` (`id`, `project_id`, `name`, `note`, `created_at`, `organization`) VALUES
(1, 1, 'Version 1', 'Version 1 is made for initial testing', '2015-08-31 14:24:03', '{"title":"Sample Organization","measurementUnit":"sft","logo":"company_logo.png","units":[{"name":"Admin","code":"ADM","area":700,"isShowArea":true,"seat":22,"isShowSeat":false,"areaPerSeat":32,"isUsed":true,"fill":{"startColor":"#303F9F","endColor":"#3F51B5"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Activity Area + Support","code":"ACT","area":1200,"isShowArea":false,"seat":40,"isShowSeat":false,"areaPerSeat":30,"isUsed":false,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Planning","code":"PLA","area":800,"isShowArea":true,"seat":32,"isShowSeat":false,"areaPerSeat":25,"isUsed":true,"fill":{"startColor":"#7B1FA2","endColor":"#9C27B0"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Planning-1","code":"PLA1","area":200,"isShowArea":true,"seat":8,"isShowSeat":false,"areaPerSeat":25,"isUsed":false,"fill":{"startColor":"#7B1FA2","endColor":"#9C27B0"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Finance","code":"FIN","area":600,"isShowArea":true,"seat":20,"isShowSeat":false,"areaPerSeat":30,"isUsed":false,"fill":{"startColor":"#8BC34A","endColor":"#4CAF50"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Information Technology","code":"IT","area":800,"isShowArea":true,"seat":20,"isShowSeat":false,"areaPerSeat":40,"isUsed":false,"fill":{"startColor":"#F44336","endColor":"#D32F2F"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Human Resource Management","code":"HRM","area":300,"isShowArea":true,"seat":10,"isShowSeat":false,"areaPerSeat":30,"isUsed":false,"fill":{"startColor":"#0097A7","endColor":"#00BCD4"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Conference Centre","code":"CON","area":500,"isShowArea":true,"seat":25,"isShowSeat":false,"areaPerSeat":20,"isUsed":false,"fill":{"startColor":"#5D4037","endColor":"#795548"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Gym","code":"GYM","area":500,"isShowArea":true,"seat":10,"isShowSeat":false,"areaPerSeat":50,"isUsed":false,"fill":{"startColor":"#C2185B","endColor":"#E91E63"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Operations","code":"OP","area":400,"isShowArea":true,"seat":20,"isShowSeat":false,"areaPerSeat":20,"isUsed":false,"fill":{"startColor":"#AFB42B","endColor":"#8BC34A"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Fun dept","code":"FD","area":1000,"isShowArea":true,"seat":0,"isShowSeat":false,"areaPerSeat":0,"isUsed":false,"fill":{"startColor":"#ffff00","endColor":"#89c66c"},"font":{"color":"#826668","fontSize":"11px"}}],"buildings":[{"title":"Tower 1","floors":[{"title":"8\\/F","area":900,"offset":100,"void":120,"core":80,"units":[]},{"title":"7\\/F","area":900,"offset":100,"void":120,"core":80,"units":[]},{"title":"6\\/F","area":900,"offset":100,"void":120,"core":80,"units":[]},{"title":"5\\/F","area":1000,"offset":0,"void":120,"core":80,"units":["PLA"]},{"title":"4\\/F","area":1000,"offset":0,"void":120,"core":80,"units":[]},{"title":"3\\/F","area":1300,"offset":0,"void":120,"core":180,"units":[]},{"title":"my floor","area":"1000","offset":"20","void":"100","core":"100","units":[]},{"title":"2\\/F","area":1300,"offset":0,"void":120,"core":0,"units":["ADM"]},{"title":"1\\/F","area":1700,"offset":0,"void":100,"core":200,"units":[]}]},{"title":"Tower 2","floors":[{"title":"8\\/F","area":1300,"offset":0,"void":120,"core":80,"units":[]},{"title":"7\\/F","area":1700,"offset":0,"void":100,"core":200,"units":[]}]},{"title":"Tower 3","floors":[{"title":"9\\/F","area":1000,"offset":0,"void":120,"core":80,"units":[]},{"title":"8\\/F","area":1300,"offset":0,"void":120,"core":180,"units":[]},{"title":"2\\/F","area":1300,"offset":0,"void":120,"core":0,"units":[]},{"title":"1\\/F","area":1700,"offset":0,"void":100,"core":200,"units":[]}]}]}'),
(2, 1, 'Version 2', 'For client', '2015-08-31 14:24:03', '{"title":"Sample Organization","measurementUnit":"sft","logo":"company_logo.png","units":[{"name":"Admin","code":"ADM","area":700,"isShowArea":true,"seat":22,"isShowSeat":false,"areaPerSeat":32,"isUsed":true,"fill":{"startColor":"#303F9F","endColor":"#3F51B5"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Activity Area + Support","code":"ACT","area":1200,"isShowArea":false,"seat":40,"isShowSeat":false,"areaPerSeat":30,"isUsed":false,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Planning","code":"PLA","area":800,"isShowArea":true,"seat":32,"isShowSeat":false,"areaPerSeat":25,"isUsed":true,"fill":{"startColor":"#7B1FA2","endColor":"#9C27B0"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Planning-1","code":"PLA1","area":200,"isShowArea":true,"seat":8,"isShowSeat":false,"areaPerSeat":25,"isUsed":false,"fill":{"startColor":"#7B1FA2","endColor":"#9C27B0"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Finance","code":"FIN","area":600,"isShowArea":true,"seat":20,"isShowSeat":false,"areaPerSeat":30,"isUsed":false,"fill":{"startColor":"#8BC34A","endColor":"#4CAF50"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Information Technology","code":"IT","area":800,"isShowArea":true,"seat":20,"isShowSeat":false,"areaPerSeat":40,"isUsed":false,"fill":{"startColor":"#F44336","endColor":"#D32F2F"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Human Resource Management","code":"HRM","area":300,"isShowArea":true,"seat":10,"isShowSeat":false,"areaPerSeat":30,"isUsed":false,"fill":{"startColor":"#0097A7","endColor":"#00BCD4"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Conference Centre","code":"CON","area":500,"isShowArea":true,"seat":25,"isShowSeat":false,"areaPerSeat":20,"isUsed":false,"fill":{"startColor":"#5D4037","endColor":"#795548"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Gym","code":"GYM","area":500,"isShowArea":true,"seat":10,"isShowSeat":false,"areaPerSeat":50,"isUsed":false,"fill":{"startColor":"#C2185B","endColor":"#E91E63"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Operations","code":"OP","area":400,"isShowArea":true,"seat":20,"isShowSeat":false,"areaPerSeat":20,"isUsed":false,"fill":{"startColor":"#AFB42B","endColor":"#8BC34A"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Fun dept","code":"FD","area":1000,"isShowArea":true,"seat":0,"isShowSeat":false,"areaPerSeat":0,"isUsed":false,"fill":{"startColor":"#ffff00","endColor":"#89c66c"},"font":{"color":"#826668","fontSize":"11px"}}],"buildings":[{"title":"Tower 1","floors":[{"title":"8\\/F","area":900,"offset":100,"void":120,"core":80,"units":[]},{"title":"7\\/F","area":900,"offset":100,"void":120,"core":80,"units":[]},{"title":"6\\/F","area":900,"offset":100,"void":120,"core":80,"units":[]},{"title":"5\\/F","area":1000,"offset":0,"void":120,"core":80,"units":["PLA"]},{"title":"4\\/F","area":1000,"offset":0,"void":120,"core":80,"units":[]},{"title":"3\\/F","area":1300,"offset":0,"void":120,"core":180,"units":[]},{"title":"2\\/F","area":1300,"offset":0,"void":120,"core":0,"units":["ADM"]},{"title":"1\\/F","area":1700,"offset":0,"void":100,"core":200,"units":[]}]},{"title":"Tower 2","floors":[{"title":"8\\/F","area":1300,"offset":0,"void":120,"core":80,"units":[]},{"title":"7\\/F","area":1700,"offset":0,"void":100,"core":200,"units":[]}]},{"title":"Tower 3","floors":[{"title":"9\\/F","area":1000,"offset":0,"void":120,"core":80,"units":[]},{"title":"8\\/F","area":1300,"offset":0,"void":120,"core":180,"units":[]},{"title":"2\\/F","area":1300,"offset":0,"void":120,"core":0,"units":[]},{"title":"1\\/F","area":1700,"offset":0,"void":100,"core":200,"units":[]}]}]}'),
(4, 11, 'February design', 'February version was made in response to client''s change request', '2015-08-31 14:26:00', '{"buildings":[{"title":"Tower 1","floors":[{"title":"11\\/F","area":700,"offset":300,"void":100,"core":100,"units":["ADM","OPS2"]},{"title":"8\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"7\\/F","area":700,"offset":0,"void":200,"core":0,"units":["OPS1"]},{"title":"3\\/F","area":1000,"offset":0,"void":100,"core":0,"units":[]},{"title":"2\\/F","area":1000,"offset":0,"void":100,"core":0,"units":[]},{"title":"1\\/F","area":1300,"offset":0,"void":100,"core":200,"units":["OPS"]}]},{"title":"Tower 2","floors":[{"title":"5\\/F","area":700,"offset":300,"void":0,"core":0,"units":[]},{"title":"4\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"3\\/F","area":700,"offset":300,"void":0,"core":0,"units":[]},{"title":"2\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"1\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"GF","area":1300,"offset":0,"void":0,"core":0,"units":[]},{"title":"Garage0","area":1300,"offset":0,"void":0,"core":0,"units":[]},{"title":"Garage1","area":1300,"offset":0,"void":0,"core":0,"units":[]},{"title":"Subway","area":2000,"offset":0,"void":400,"core":100,"units":[]}]}],"units":[{"name":"Administration","code":"ADM","area":400,"isShowArea":true,"seat":20,"isShowSeat":true,"areaPerSeat":20,"isUsed":true,"fill":{"startColor":"#303F9F","endColor":"#3F51B5"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Operations","code":"OPS","area":1000,"isShowArea":false,"seat":18,"isShowSeat":true,"areaPerSeat":55,"isUsed":true,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Operations-1","code":"OPS1","area":500,"isShowArea":false,"seat":9,"isShowSeat":true,"areaPerSeat":55,"isUsed":true,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Operations-2","code":"OPS2","area":150,"isShowArea":false,"seat":3,"isShowSeat":true,"areaPerSeat":55,"isUsed":true,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"}}]}'),
(5, 16, 'With parking', 'This design adds parking space', '2015-08-31 14:26:58', '{"title":"dfgdfg","measurementUnit":"sqm","logo":"company_logo.png","units":[{"name":"Administration","code":"ADM","area":400,"isShowArea":true,"seat":20,"isShowSeat":true,"areaPerSeat":20,"isUsed":false,"fill":{"startColor":"#303F9F","endColor":"#3F51B5"},"font":{"color":"#ffffff","fontSize":"11px"},"$$hashKey":"object:10"},{"name":"Operations","code":"OPS","area":1650,"isShowArea":false,"seat":30,"isShowSeat":true,"areaPerSeat":55,"isUsed":false,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"},"$$hashKey":"object:11"}],"buildings":[{"title":"Tower 1","floors":[{"title":"11/F","area":700,"offset":300,"void":100,"core":100,"units":[],"$$hashKey":"object:18"},{"title":"8/F","area":1000,"offset":0,"void":0,"core":0,"units":[],"$$hashKey":"object:19"},{"title":"7/F","area":700,"offset":0,"void":200,"core":0,"units":[],"$$hashKey":"object:20"},{"title":"3/F","area":1000,"offset":0,"void":100,"core":0,"units":[],"$$hashKey":"object:21"},{"title":"2/F","area":1000,"offset":0,"void":100,"core":0,"units":[],"$$hashKey":"object:22"},{"title":"1/F","area":1300,"offset":0,"void":100,"core":200,"units":[],"$$hashKey":"object:23"}],"$$hashKey":"object:14"},{"title":"Tower 2","floors":[{"title":"5/F","area":700,"offset":300,"void":0,"core":0,"units":[],"$$hashKey":"object:30"},{"title":"4/F","area":1000,"offset":0,"void":0,"core":0,"units":[],"$$hashKey":"object:31"},{"title":"3/F","area":700,"offset":300,"void":0,"core":0,"units":[],"$$hashKey":"object:32"},{"title":"2/F","area":1000,"offset":0,"void":0,"core":0,"units":[],"$$hashKey":"object:33"},{"title":"1/F","area":1000,"offset":0,"void":0,"core":0,"units":[],"$$hashKey":"object:34"},{"title":"GF","area":1300,"offset":0,"void":0,"core":0,"units":[],"$$hashKey":"object:35"},{"title":"Garage0","area":1300,"offset":0,"void":0,"core":0,"units":[],"$$hashKey":"object:36"},{"title":"Garage1","area":1300,"offset":0,"void":0,"core":0,"units":[],"$$hashKey":"object:37"},{"title":"Subway","area":2000,"offset":0,"void":400,"core":100,"units":[],"$$hashKey":"object:38"}],"$$hashKey":"object:15"}]}'),
(6, 11, 'January Design', 'January design for client 1', '2015-09-25 13:29:01', '{"title":"dfgdfg","measurementUnit":"sqm","logo":"company_logo.png","units":[{"name":"Administration","code":"ADM","area":400,"isShowArea":true,"seat":20,"isShowSeat":true,"areaPerSeat":20,"isUsed":false,"fill":{"startColor":"#303F9F","endColor":"#3F51B5"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"Operations","code":"OPS","area":1650,"isShowArea":false,"seat":30,"isShowSeat":true,"areaPerSeat":55,"isUsed":false,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"}},{"name":"February","code":"FEB","area":1650,"isShowArea":false,"seat":30,"isShowSeat":true,"areaPerSeat":55,"isUsed":false,"fill":{"startColor":"#FBC02D","endColor":"#FFEB3B"},"font":{"color":"#ffffff","fontSize":"11px"}}],"buildings":[{"title":"Tower 1","floors":[{"title":"11\\/F","area":700,"offset":300,"void":100,"core":100,"units":[]},{"title":"8\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"7\\/F","area":700,"offset":0,"void":200,"core":0,"units":[]},{"title":"3\\/F","area":1000,"offset":0,"void":100,"core":0,"units":[]},{"title":"2\\/F","area":1000,"offset":0,"void":100,"core":0,"units":[]},{"title":"1\\/F","area":1300,"offset":0,"void":100,"core":200,"units":[]},{"title":0,"area":1300,"offset":0,"void":100,"core":200,"units":[]}]},{"title":"Tower 2","floors":[{"title":"5\\/F","area":700,"offset":300,"void":0,"core":0,"units":[]},{"title":"4\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"3\\/F","area":700,"offset":300,"void":0,"core":0,"units":[]},{"title":"2\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"1\\/F","area":1000,"offset":0,"void":0,"core":0,"units":[]},{"title":"GF","area":1300,"offset":0,"void":0,"core":0,"units":[]},{"title":"Garage0","area":1300,"offset":0,"void":0,"core":0,"units":[]},{"title":"Garage1","area":1300,"offset":0,"void":0,"core":0,"units":[]},{"title":"Subway","area":2000,"offset":0,"void":400,"core":100,"units":[]}]}]}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `versions`
--
ALTER TABLE `versions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `versions`
--
ALTER TABLE `versions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
